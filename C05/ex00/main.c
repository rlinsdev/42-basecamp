/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rlins <rlins@student.42sp.org.br>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/20 15:51:42 by rlins             #+#    #+#             */
/*   Updated: 2022/02/20 15:51:42 by rlins            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>
#include "ft_iterative_factorial.c"

int main(void)
{
  //printf("oi");
  printf("Result: %d \n", ft_iterative_factorial(5));
  return (0);
}